<?php

namespace Avris\Deployer;

abstract class Config
{
    abstract public function repositoryUrl(): string;

    public function branch(): string
    {
        return 'master';
    }

    public function keepReleases(): int
    {
        return 3;
    }

    public function sharedFiles(): iterable
    {
        return [];
    }

    public function removeFiles(): iterable
    {
        yield '.git';
    }

    public function before(): iterable
    {
        return [];
    }

    public function make(): iterable
    {
        yield ['make', 'deploy'];
    }

    public function after(): iterable
    {
        return [];
    }

    public function beforeRemove(): iterable
    {
        return [];
    }

    public function finish(): iterable
    {
        return [];
    }

    public function switchVersion(): iterable
    {
        return [];
    }
}
