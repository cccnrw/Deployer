<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitCommand extends Command
{
    protected static $defaultName = 'init';

    /** @var Deployer */
    private $deployer;

    public function __construct(Deployer $deployer)
    {
        parent::__construct(null);
        $this->deployer = $deployer;
    }

    protected function configure()
    {
        $this->addArgument('repo', InputArgument::REQUIRED, 'URL of the GIT repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $projectDir = $this->deployer->init($input->getArgument('repo'));

        $output->writeln(sprintf('Initialised a deployer configuration in <info>%s</info>', $projectDir));
        $output->writeln('');

        $output->writeln('Please modify <info>deploy.php</info> and <info>shared</info> to match your needs.');
        $output->writeln('');

        $output->writeln(sprintf('Please make sure your project (<info>%s</info>) has a <info>Makefile</info> with a <info>deploy</info> directive. For example:', $input->getArgument('repo')));
        $output->writeln(<<<TEXT
    deploy:
        composer install --no-dev --optimize-autoloader
        bin/console doctrine:migration:migrate -n
        yarn
        yarn build
TEXT
        );
        $output->writeln('');

        $output->write("\x07");

        return 0;
    }
}
