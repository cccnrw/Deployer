<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

final class Deployer
{
    const TIMESTAMP_FORMAT = 'YmdHis';

    /** @var Filesystem */
    private $filesystem;

    /** @var OutputInterface */
    private $output;

    /** @var Config */
    private $config;

    /** @var string */
    private $projectDir;

    /** @var string */
    private $currentLink;

    /** @var string */
    private $releasesDir;

    /** @var string */
    private $sharedDir;

    public function __construct(string $projectDir, OutputInterface $output)
    {
        $this->projectDir = $projectDir;
        $this->output = $output;

        $this->filesystem = new Filesystem();

        $this->configFile = $this->projectDir . '/deploy.php';
        $this->currentLink = $this->projectDir . '/current';
        $this->releasesDir = $this->projectDir . '/release';
        $this->sharedDir = $this->projectDir . '/shared';

    }

    private function configure()
    {
        if (!$this->filesystem->exists($this->configFile)) {
            throw new \RuntimeException(sprintf(
                'Cannot deploy in directory %s - file deploy.php not found',
                $this->projectDir
            ));
        }

        $this->config = require_once $this->configFile;
    }

    public function deploy(?string $branch)
    {
        $this->configure();

        $start = microtime(true);

        $releaseDir = $this->releasesDir . '/'  .date(self::TIMESTAMP_FORMAT);

        $this->step('Project directory: ' . $this->projectDir);

        $this->filesystem->mkdir($this->releasesDir);
        $this->filesystem->mkdir($this->sharedDir);

        $branch = $branch ?: $this->config->branch();
        $this->step(sprintf('Cloning %s (%s)', $this->config->repositoryUrl(), $branch));
        $this->exec(['git', 'clone', $this->config->repositoryUrl(), $releaseDir]);
        $this->exec(['git', 'checkout', $branch], $releaseDir);

        foreach ($this->config->before() as $command) {
            $this->exec($command, $releaseDir);
        }

        $this->step('Linking shared files');
        foreach ($this->config->sharedFiles() as $from => $to) {
            if (is_int($from)) {
                $from = $to;
            }

            $this->debug(sprintf('Linking %s → %s', $from, $to));
            $this->filesystem->remove($releaseDir . '/' . $to);
            $this->filesystem->symlink($this->sharedDir . '/' . $from, $releaseDir . '/' . $to);
        }

        foreach ($this->config->make() as $command) {
            $this->exec($command, $releaseDir);
        }

        foreach ($this->config->after() as $command) {
            $this->exec($command, $releaseDir);
        }

        $this->step('Removing unnecessary files');
        foreach ($this->config->removeFiles() as $fileToRemove) {
            $this->debug(sprintf('Removing %s', $fileToRemove));
            $this->filesystem->remove($releaseDir . '/' . $fileToRemove);
        }

        $keepReleases = max(1, $this->config->keepReleases());
        $this->step(sprintf('Removing old releases (keep %s)', $keepReleases));
        $releasesToRemove = array_slice(
            glob($this->releasesDir . '/*'),
            0,
            -$keepReleases
        );
        foreach ($releasesToRemove as $release) {
            $this->debug(sprintf('Removing relese %s', $release));
            foreach ($this->config->beforeRemove() as $command) {
                $this->exec($command, $release);
            }
            $this->filesystem->remove($release);
        }

        $this->step('Linking current');
        $this->filesystem->symlink($releaseDir, $this->currentLink);

        foreach ($this->config->finish() as $command) {
            $this->exec($command, $this->currentLink);
        }

        $this->step('Successfully deployed');
        $stop = microtime(true);
        $duration = $stop - $start;
        $this->output->writeln(sprintf('Time taken: %.2fs', $duration) . ($duration >= 70 ? sprintf(' (%.2fm)', $duration  / 60) : ''));
    }

    private function step(string $description)
    {
        $this->output->writeln('<info>→ ' . $description. '</info>');
    }

    private function debug(string $message)
    {
        $this->output->writeln($message, OutputInterface::VERBOSITY_VERBOSE);
    }

    private function exec(array $command, ?string $cwd = null)
    {
        $this->step('Exec: ' . join(' ', $command));

        $process = new Process($command, $cwd, null, null, null);
        $process->enableOutput();
        $process->mustRun(function (string $type, string $buffer) {
            if (!$this->output->isVerbose()) {
                return;
            }

            if ($type === Process::ERR) {
                $this->output->write('<error>' . $buffer . '</error>');
            } else {
                $this->output->write($buffer);
            }
        });
    }

    public function listVersions(): int
    {
        $this->configure();

        $activeIndex = -1;

        $active = $this->filesystem->readlink($this->currentLink);
        foreach (glob($this->releasesDir . '/*') as $i => $dir) {
            $formatted = $this->formatVersion($dir);
            if ($dir === $active) {
                $this->output->writeln('<info> → ' . $formatted. '</info>');
                $activeIndex = $i;
            } else {
                $this->output->writeln(' – ' . $formatted);
            }
        }

        return $activeIndex;
    }

    public function switchVersion(int $newIndex): ?string
    {
        foreach (glob($this->releasesDir . '/*') as $i => $dir) {
            if ($i === $newIndex) {
                $this->filesystem->remove($this->currentLink);
                $this->filesystem->symlink($dir, $this->currentLink);

                foreach ($this->config->switchVersion() as $command) {
                    $this->exec($command, $this->currentLink);
                }

                return $this->formatVersion($dir);
            }
        }

        return null;
    }

    private function formatVersion($path)
    {
        return \DateTimeImmutable::createFromFormat(self::TIMESTAMP_FORMAT, basename($path))
            ->format('Y-m-d H:i:s');
    }

    public function init(string $repo): string
    {
        if ($this->filesystem->exists($this->configFile)) {
            throw new \RuntimeException(sprintf(
                'Deployer is already initialised in directory %s',
                $this->projectDir
            ));
        }

        $this->filesystem->mkdir($this->sharedDir);
        $this->filesystem->dumpFile($this->sharedDir . '/.env.local', 'APP_ENV=prod' . PHP_EOL);
        $this->filesystem->mkdir($this->sharedDir . '/var/log');

        $this->filesystem->dumpFile(
            $this->configFile,
            <<<PHP
<?php

return new class extends \Avris\Deployer\Config
{
        public function repositoryUrl(): string
        {
                return '$repo';
        }

        public function sharedFiles(): iterable
        {
                yield '.env.local';
                yield 'var/log';
        }

        public function removeFiles(): iterable
        {
                yield '.git';
                yield 'node_modules';
        }
};
PHP
        );

        return $this->projectDir;
    }
}
