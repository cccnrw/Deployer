<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeployCommand extends Command
{
    protected static $defaultName = 'deploy';

    /** @var Deployer */
    private $deployer;

    public function __construct(Deployer $deployer)
    {
        parent::__construct(null);
        $this->deployer = $deployer;
    }

    protected function configure()
    {
        $this->setAliases(['d']);
        $this->addArgument('branch', InputArgument::OPTIONAL, 'git branch to check out');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->deployer->deploy($input->getArgument('branch'));
        $output->write("\x07");

        return 0;
    }
}
