<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VersionCommand extends Command
{
    protected static $defaultName = 'version';

    /** @var Deployer */
    private $deployer;

    public function __construct(Deployer $deployer)
    {
        parent::__construct(null);
        $this->deployer = $deployer;
    }

    protected function configure()
    {
        $this->addArgument('action', InputArgument::OPTIONAL, 'list (default) | prev | next');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $activeIndex = $this->deployer->listVersions();
        $move = 0;

        switch ($input->getArgument('action') ?: 'list') {
            case 'list':
                break;
            case 'prev':
                $move = -1;
                break;
            case 'next':
                $move = 1;
                break;
            default:
                throw new InvalidArgumentException('Invalid action');

        }

        if (!$move) {
            return 0;
        }

        $result = $this->deployer->switchVersion($activeIndex + $move);
        if ($result) {
            $output->writeln('');
            $output->writeln(sprintf('<info>New version: %s</info>', $result));
            $output->writeln('');
            $this->deployer->listVersions();
        } else {
            $output->writeln('');
            $output->writeln('<error>No version to switch to</error>');
            $output->writeln('');
        }

        $output->write("\x07");

        return 0;
    }
}
