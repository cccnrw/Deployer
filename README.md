# Avris Deployer

Simple deployment helper. This script:

 - clones a given repository and branch
 - (optionally) runs some `before` commands
 - links files shared between deployements (like logs or `.env`)
 - executes `make deploy`
 - (optionally) runs some `after` commands
 - removes unnecessary files (like `.git`)
 - cleans up old releases (by default keeps 3 most recent ones)
 - links the release to the `current` directory
 - (optionally) runs some `finish` commands

## Installation

    composer global require avris/deployer

## Usage

First, make sure all your essential project setup can be done via `make deploy`.
An example `Makefile` in your repository:

    deploy:
        composer install --no-dev --optimize-autoloader
        bin/console doctrine:migration:migrate -n
        yarn
        yarn build

Second, create a folder to which you will deploy and initialise Deployer:

    mdkir project_name
    cd project_name
    deployer init <repo_url>

This command will create a directory `shared`. Put there all the files that are supposed to be shared between deployments.

It will also create `deploy.php` file. This file holds all the configuration of the deployment,
and it looks like this: 

    <?php
    
    return new class extends \Avris\Deployer\Config
    {
            public function repositoryUrl(): string
            {
                    return 'git@gitlab.com:Avris/OurSong.git';
            }
    
            public function sharedFiles(): iterable
            {
                    yield '.env';
                    yield 'keys';
                    yield 'server/.env.local';
                    yield 'server/var/geo.json';
                    yield 'server/var/log';
            }
    
            public function finish(): iterable
            {
                    yield ['sudo', 'service', 'php7.1-fpm', 'restart'];
            }
    };

Only `repositoryUrl` is required. Check out `Avris\Deployer\Config` for all the config and hook options.

You can now execute a deployment: just run `deployer` (or `deployer <branch>` if you want to overwrite the default branch).

To switch between versions run `deployer version list|prev|next`.

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
